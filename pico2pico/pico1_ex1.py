# code for pico1

import machine
import utime

#init HW
pico1_in1 = machine.Pin(15, machine.Pin.IN, machine.Pin.PULL_DOWN) #connected to pico1 pin5
pico1_out1 = machine.Pin(14, machine.Pin.OUT) #connected to pico1 pin4
led_onboard = machine.Pin(25, machine.Pin.OUT)

# init state
led_onboard.value(0)

while True:
        led_onboard.value(1)
        pico1_out1.value(1)
        utime.sleep(1)
        
        led_onboard.value(0)
        pico1_out1.value(0)
        utime.sleep(1)
