# code for pico2 - receiver

import machine
import utime

# init HW
pico2_in1 = machine.Pin(4, machine.Pin.IN, machine.Pin.PULL_DOWN) #connected to pico1 pin15
pico2_in2 = machine.Pin(5, machine.Pin.IN, machine.Pin.PULL_DOWN) #connected to pico1 pin14
led_onboard = machine.Pin(25, machine.Pin.OUT)

#init state
led_onboard.value(0)

def blink(times):
    for i in range(times):
        led_onboard.value(1)
        utime.sleep(0.1)
        led_onboard.value(0)
        utime.sleep(0.1)

while True:
        if pico2_in1.value() == 1:
            print('received from pico1 on input 1')
            blink(1)
        
        elif pico2_in2.value() == 1:
            print('received from pico1 on input 2')
            blink(2)
            
        elif pico2_in1.value() and pico2_in2.value() == 1:
            print('received from pico1 on input 1+2')
            blink(3)

        
        
