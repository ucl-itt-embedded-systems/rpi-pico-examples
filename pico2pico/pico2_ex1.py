# code for pico2

import machine
import utime

# init HW
pico2_out1 = machine.Pin(5, machine.Pin.OUT) #connected to pico1 pin15
pico2_in1 = machine.Pin(4, machine.Pin.IN, machine.Pin.PULL_UP) #connected to pico1 pin14
led_onboard = machine.Pin(25, machine.Pin.OUT)

#init state
led_onboard.value(0)
pico2_out1.value(0)

while True:
    if pico2_in1.value() == 0:
        led_onboard.value(0)
    else:
        led_onboard.value(1)
        
