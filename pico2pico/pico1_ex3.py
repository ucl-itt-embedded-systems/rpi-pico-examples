# code for pico1 - sender

import machine
import utime

# init HW
pico1_out1 = machine.Pin(14, machine.Pin.OUT) #connected to pico2 pin4
pico1_out2 = machine.Pin(15, machine.Pin.OUT) #connected to pico2 pin5
btn1 = machine.Pin(18, machine.Pin.IN, machine.Pin.PULL_UP)
btn2 = machine.Pin(19, machine.Pin.IN, machine.Pin.PULL_UP)
led_onboard = machine.Pin(25, machine.Pin.OUT)

#init state
led_onboard.value(0)
pico1_out1.value(0)
pico1_out2.value(0)
    

while True:
    if btn1.value() == 0:
        print('1 pushed')
        utime.sleep(0.5)
        pico1_out1.value(1)
        utime.sleep(0.1)
        pico1_out1.value(0)
    
    elif btn2.value() == 0:
        print('2 pushed')
        utime.sleep(0.5)
        pico1_out2.value(1)
        utime.sleep(0.1)
        pico1_out2.value(0)
        
    while btn1.value() and btn2.value() == 0:
        print('both pushed')
        utime.sleep(0.5)
        pico1_out1.value(1)
        pico1_out2.value(1)
        utime.sleep(0.1)
        pico1_out1.value(0)
        pico1_out2.value(0)
