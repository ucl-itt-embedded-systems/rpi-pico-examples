'''
From the book https://hackspace.raspberrypi.org/books/micropython-pico chapter 2
'''
user_name = input ("What is your name? ")
while user_name != "Clark Kent":
    print("You are not Superman - try again!")
    user_name = input("What is your name? ")
    print("You are Superman!")
