'''
From the book https://hackspace.raspberrypi.org/books/micropython-pico chapter 2
'''

user_name = input ("What is your name? ")
if user_name == "Clark Kent":
    print("You are Superman!")
else:
    print("You are not Superman!")
