'''
From the book https://hackspace.raspberrypi.org/books/micropython-pico chapter 2
'''
number = input("input a number ")
while int(number) <= 5:
    print("The number " + number + " is below or equal to 5")
    number = input("input a new number ")
if int(number) > 5:
    print("The number " + number + " is above 5")