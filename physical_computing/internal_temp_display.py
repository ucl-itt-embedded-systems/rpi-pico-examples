'''
This program uses the internal temperature sensor of the RP2040
and outputs the ADC raw reading, the converted ADC value and the calculated temperature in Celcius
to a 128x64 pixel oled display controlled by a ssd1306 driver

Needed libraries on the Pico: ssd1306, display_helper
'''

#Libraries
import machine
import display_helper as disp
import utime

#Init
sensor_temp = machine.ADC(4)

#Variables
conversion_factor = 3.3 / (65535)

#Functions
def init():
    disp.clear()
    disp.write_block(
        'This program    shows the   internal temperature of the RP2040'
        )
    disp.show()
    utime.sleep(1)

#read the sensor many times and return mean
def mean_reading():
    adc_val = 0
    for i in range(500):
        adc_val += sensor_temp.read_u16()
        utime.sleep(0.01)
    return adc_val/500
    

#Main program
init()
while True:
    disp.clear()
    disp.write_line('INTERNAL TEMP', 1)
    # raw_reading = sensor_temp.read_u16()
    raw_reading = mean_reading()
    disp.write_line('Raw ADC:' + str(round(raw_reading, 2)), 2)
    reading = raw_reading * conversion_factor
    disp.write_line('Convert:' + str(round(reading, 4)), 3)
    temperature = 27 - (reading - 0.706)/0.001721
    disp.write_line('Celcius:' + str(round(temperature, 2)), 4)
    disp.write_line('Room ~ :' + str(round(temperature-3, 2)), 5)
    disp.show()
    utime.sleep(3)