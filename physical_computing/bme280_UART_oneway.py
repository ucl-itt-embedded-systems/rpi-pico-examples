# BME280 library https://github.com/SebastianRoll/mpy_bme280_esp8266

from machine import SoftI2C, Pin, UART
import bme280 as bme280 # the bme280 library 
from utime import sleep

i2c = SoftI2C(scl=Pin(27), sda=Pin(26), freq=10000)
#i2c = I2C(0, scl=Pin(1), sda=Pin(0), freq=10000) # default assignment: scl=Pin(9), sda=Pin(8)


#Print out any addresses found
# devices = i2c.scan()
# if devices:
#     for d in devices:
#         print(hex(d)

uart0 = UART(0, baudrate=9600, tx=Pin(16), rx=Pin(17))     
bme = bme280.BME280(i2c=i2c, address=0x77)
led_onboard = machine.Pin(25, machine.Pin.OUT)


def led_blink(led, times, freq):
    for i in range(times):
        led.value(1)
        sleep(freq)
        led.value(0)
        sleep(freq)
        
led_blink(led=led_onboard, times=3, freq=0.1)

while(True):
    
    # (temperature, pressure, humidity) = bme.values
    # print(str(bme.values) + '\n') # development debug message
    uart0.write(str(bme.values) + '\n')
    sleep(2)
    led_blink(led=led_onboard, times=1, freq=0.2)
