# BME280 library https://github.com/SebastianRoll/mpy_bme280_esp8266
# The library needs to be put in the lib folder of the Pico
# To run this file on Pico boot save it on the Pico as main.py (outside of the lib folder)


from machine import SoftI2C, Pin, UART
import bme280 as bme280 # the bme280 library 
from utime import sleep

i2c = SoftI2C(scl=Pin(27), sda=Pin(26), freq=10000)

#Print out any addresses found
# devices = i2c.scan()
# if devices:
#     for d in devices:
#         print(hex(d)

uart0 = UART(0, 19200, bits=8, parity=None, stop=1, tx=Pin(16), rx=Pin(17))     
bme = bme280.BME280(i2c=i2c, address=0x77)
led_onboard = machine.Pin(25, machine.Pin.OUT)


def led_blink(led, times, freq):
    for i in range(times):
        led.value(1)
        sleep(freq)
        led.value(0)
        sleep(freq)
        
led_blink(led=led_onboard, times=3, freq=0.1)

while(True):
    (temperature, pressure, humidity) = bme.values # unpack tuple to variables
    # print(str(bme.values) + '\n') # development debug message

    if uart0.any() > 0:
        msg = uart0.readline() or b'' # read UART, if nothing received set msg to empty byte object
        msg = msg.decode('utf-8').rstrip() #decode byte object and remove newline character
        if msg == 'temperature':
            uart0.write((temperature) + '\n')
            print(temperature)
            led_blink(led=led_onboard, times=1, freq=0.2)
        elif msg == 'pressure':
            uart0.write((pressure) + '\n')
            print(pressure)
            led_blink(led=led_onboard, times=2, freq=0.2)
        elif msg == 'humidity':
            uart0.write((humidity) + '\n')
            print(humidity)
            led_blink(led=led_onboard, times=3, freq=0.2)
        else:
            uart0.write(('error') + '\n')
            led_blink(led=led_onboard, times=5, freq=0.05)

