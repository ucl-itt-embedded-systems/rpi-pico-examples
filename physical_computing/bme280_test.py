# BME280 library https://github.com/SebastianRoll/mpy_bme280_esp8266

from machine import SoftI2C, Pin
import bme280 as bme280 # the bme280 library 
from utime import sleep

i2c = SoftI2C(scl=Pin(27), sda=Pin(26), freq=10000)
#i2c = I2C(0, scl=Pin(1), sda=Pin(0), freq=10000) # default assignment: scl=Pin(9), sda=Pin(8)


#Print out any addresses found
# devices = i2c.scan()
# if devices:
#     for d in devices:
#         print(hex(d))
      
      
bme = bme280.BME280(i2c=i2c, address=0x77)

while(True):

    (temperature, pressure, humidity) = bme.values
    print(temperature)
    print(pressure)
    print(humidity)
    sleep(1)

        



