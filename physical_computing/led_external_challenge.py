'''
From the book https://hackspace.raspberrypi.org/books/micropython-pico chapter 4
Example is using 2 external led's + the onboard led
'''
# library import
import machine
import utime

# global variables
led_onboard = machine.Pin(25, machine.Pin.OUT)
led_external_D1 = machine.Pin(18, machine.Pin.OUT)
led_external_D2 = machine.Pin(19, machine.Pin.OUT)

# initialize
led_onboard.value(0)
led_external_D1.value(0)
led_external_D2.value(0)

# main program
while True:    
    led_external_D2.toggle() 
    utime.sleep(0.1)
    led_external_D1.toggle()    
    utime.sleep(0.1)
    led_onboard.toggle()
    utime.sleep(0.1)
 