'''
From the book https://hackspace.raspberrypi.org/books/micropython-pico chapter 4
The program is using 2 buttons that connects to ground when pushed
'''

# library import
import machine
import utime

# global variables
button1 = machine.Pin(16, machine.Pin.IN, machine.Pin.PULL_UP)
button2 = machine.Pin(17, machine.Pin.IN, machine.Pin.PULL_UP)

# initialize

# main program
while True:
    if button1.value() == 0:
        print("You pressed button1")
        utime.sleep(0.3)
    elif button2.value() == 0:
        print("You pressed button2")
        utime.sleep(0.3)
