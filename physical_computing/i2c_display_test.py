# i2c_display_test

# libraries
from machine import Pin, I2C
from ssd1306 import SSD1306_I2C
from display_helper import DisplayHelp
import utime

i2c = I2C(1, scl=Pin(27), sda=Pin(26), freq=400000) 
#display_object = SSD1306_I2C(128, 64, i2c) #width, height, i2c, addr=0x3C, external_vcc=False)
disp = DisplayHelp(SSD1306_I2C(128, 64, i2c), color=1, line_length=16, line_heigth=10, debug=False)
while True:

    disp.write_block("This is a longer block of text, showing that works. yada, yada, yada, yada, yada, yada!")
    disp.show()
    utime.sleep(2)
    disp.clear()
