'''
From the book https://hackspace.raspberrypi.org/books/micropython-pico chapter 4
'''
# library import
import machine
import utime

# global variables
led_external_D1 = machine.Pin(18, machine.Pin.OUT)

# initialize
led_external_D1.value(0)

# main program
while True:     
    led_external_D1.toggle() 
    utime.sleep(0.1)