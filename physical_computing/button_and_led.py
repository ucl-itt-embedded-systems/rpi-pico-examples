'''
From the book https://hackspace.raspberrypi.org/books/micropython-pico chapter 4
The program is using 2 buttons that connects to ground when pushed + 2 external led's
'''

# library import
import machine
import utime

# global variables
button1 = machine.Pin(14, machine.Pin.IN, machine.Pin.PULL_UP)
button2 = machine.Pin(15, machine.Pin.IN, machine.Pin.PULL_UP)
led_onboard = machine.Pin(25, machine.Pin.OUT)
led_external_D1 = machine.Pin(18, machine.Pin.OUT)
led_external_D2 = machine.Pin(19, machine.Pin.OUT)


# initialize
led_onboard.value(0)
led_external_D1.value(0)
led_external_D2.value(0)

# main program
while True:
    if button1.value() == 0:
        print("You pressed button1")
        led_external_D1.toggle()
        utime.sleep(0.3)
    elif button2.value() == 0:
        print("You pressed button2")
        led_external_D2.toggle()
        utime.sleep(0.3)
