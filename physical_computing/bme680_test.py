# BME280 library https://github.com/SebastianRoll/mpy_bme280_esp8266

from machine import SoftI2C, Pin, I2C
from bme680 import *
from utime import sleep

i2c = SoftI2C(scl=Pin(1), sda=Pin(0), freq=10000)
#i2c = I2C(0, scl=Pin(1), sda=Pin(0), freq=10000) # default assignment: scl=Pin(9), sda=Pin(8)


#Print out any addresses found
# devices = i2c.scan()
# if devices:
#     for d in devices:
#         print(hex(d))
      
      
bme680 = BME680_I2C(i2c=i2c, address=0x77)
# 
for _ in range(3):
    print(bme680.temperature, bme680.humidity, bme680.pressure, bme680.gas)
    sleep(1)

# while True:
#     print(bme680.values)
#     print(bme680.dew_point)
#     sleep(1)      


        

