'''
This program uses the internal temperature sensor of the RP2040
and outputs the ADC raw reading, the converted ADC value and the calculated temperature in Celcius
'''

#Libraries
import machine
import utime

#Init
sensor_temp = machine.ADC(4)

#Variables
conversion_factor = 3.3 / (65535)

#Main program
def init():
    print('This program shows the internal temperature of the RP2040')
    utime.sleep(5)

while True:
    print('INTERNAl TEMP')
    raw_reading = sensor_temp.read_u16()
    print('Raw ADC: ' + str(raw_reading))
    reading = raw_reading * conversion_factor
    print('Convert: ' + str(round(reading, 2)))
    temperature = 27 - (reading - 0.706)/0.001721
    print('Celcius: ' + str(round(temperature, 2)))
    utime.sleep(1)