'''
From the book https://hackspace.raspberrypi.org/books/micropython-pico chapter 2
'''

# libraries
import utime

# main program
print("Loop starting!")
while True:
    print("Loop running!")
utime.sleep(1)
print("Loop finished!")
