from machine import Pin, I2C, SoftI2C

#i2c = I2C(0, scl=Pin(27), sda=Pin(26), freq=400_000)
i2c = SoftI2C(scl=Pin(27), sda=Pin(26), freq=10000)

# Print out any addresses found
devices = i2c.scan()

if devices:
    for d in devices:
        print(hex(d))