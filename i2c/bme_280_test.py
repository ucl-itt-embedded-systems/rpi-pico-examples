# BME280 library https://github.com/SebastianRoll/mpy_bme280_esp8266

from machine import Pin, I2C
import machine
import bme280

i2c = I2C(0, scl=Pin(5), sda=Pin(4), freq=400_000)
bme = bme280.BME280(i2c=i2c)

# Print out any addresses found
devices = i2c.scan()

if devices:
    for d in devices:
        print(hex(d))
        
print(bme.values)
        

