from machine import Timer
import machine

machine.freq(240000000) # set the CPU frequency to 240 MHz
print(machine.freq())

def cb1():
    print('tim done')
    
def cb2():
    print('timer 2 hello')

tim = Timer(period=2000, mode=Timer.PERIODIC, callback=lambda t:cb2())
# tim.init(period=2000, mode=Timer.ONE_SHOT, callback=lambda t:print(2))

