'''
From the book https://hackspace.raspberrypi.org/books/micropython-pico chapter 2
'''

print('loop starting')
for i in range(10):
    print('loop number', i)
print('loop finished')