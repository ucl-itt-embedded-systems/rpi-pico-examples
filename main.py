'''
This program uses the internal temperature sensor of the RP2040
and outputs the ADC raw reading, the converted ADC value and the calculated temperature in Celcius
to a 128x64 pixel oled display controlled by a ssd1306 driver

Needed libraries on the Pico: ssd1306, DisplayHelp
'''

#Libraries
import machine
from machine import Timer, Pin, SPI, SoftI2C
from ssd1306 import SSD1306_SPI
from display_helper import DisplayHelp
import time
import bme280

#Init
sensor_temp = machine.ADC(4)
button1 = machine.Pin(13, machine.Pin.IN, machine.Pin.PULL_UP)
button2 = machine.Pin(12, machine.Pin.IN, machine.Pin.PULL_UP)
led_onboard = machine.Pin(25, machine.Pin.OUT)
cs = Pin(16)    # chip select, some modules do not have a pin for this
dc = Pin(17)    # data/command
sck = Pin(18)   # closk
mosi = Pin(19)  # mosi
rst = Pin(20)   # reset
spi = SPI(0, 100000, mosi=mosi, sck=sck)
i2c = SoftI2C(scl=Pin(1), sda=Pin(0), freq=10000)

disp = DisplayHelp(SSD1306_SPI(128, 64, spi, dc, rst, cs), color=2, line_length=16, line_heigth=12, debug=False)

# Print out any addresses found
devices = i2c.scan()

if devices:
    for d in devices:
        print(hex(d))
        
bme = bme280.BME280(i2c=i2c, address=0x77)



#Functions
def disp_init():
    disp.clear()
    disp.write_block(
        'Bosch BME280      reading temperature, humidity, barometric pressure and dewpoint'
        )
    disp.show()
    time.sleep(1)

#read the sensor many times and return mean
def mean_reading():
    #start = time.ticks_ms() # get millisecond counter
    adc_val = 0
    for i in range(500):
        adc_val += sensor_temp.read_u16()
    #print(time.ticks_diff(time.ticks_ms(), start))
    return adc_val/500

def now_ms():
    ms = int(time.time() * 1000)
    return ms

#Variables
conversion_factor = 3.3 / (65535)
loop_duration = 3000 # in milliseconds
loop_start = now_ms()

#Timers
#dim_timer = Timer(period=2000, mode=Timer.ONE_SHOT, callback=lambda t:disp_dim())

#Main program
disp_init()
tim_dim_init = Timer(period=5000, mode=Timer.ONE_SHOT, callback=lambda t:disp.dim())
tim_off_init = Timer(period=10000, mode=Timer.ONE_SHOT, callback=lambda t:disp.off())
while True:
    if (now_ms() - loop_start > loop_duration):
    # print(str(now_ms()))
        disp.clear()
#         disp.write_line('INTERNAL TEMP', 1)
#         # raw_reading = sensor_temp.read_u16()
#         raw_reading = mean_reading()
#         disp.write_line('Raw ADC:' + str(round(raw_reading, 2)), 2)
#         reading = raw_reading * conversion_factor
#         disp.write_line('Convert:' + str(round(reading, 4)), 3)
#         temperature = 27 - (reading - 0.706)/0.001721
#         disp.write_line('Celcius:' + str(round(temperature, 2)), 4)
#         disp.write_line('Room ~ :' + str(round(temperature-1.4, 2)), 5)
        disp.write_line('Bosch BME280', 1)
        (temp, pressure, humidity) = bme.values
        disp.write_line('Temp ' + temp, 2)
        disp.write_line('humd ' + humidity, 3)
        disp.write_line('pres ' + pressure, 4)
        disp.write_line('dewp ' + str(round(bme.dew_point, 2)) + 'C', 5)
        #print(str(bme.sealevel))
        disp.show()
        loop_start = now_ms()
        continue
    else:
        if button1.value() == 0:
            #print("You pressed button1")
            disp.wake()
            tim_dim = Timer(period=5000, mode=Timer.ONE_SHOT, callback=lambda t:disp.dim())
            tim_off = Timer(period=10000, mode=Timer.ONE_SHOT, callback=lambda t:disp.off())
            #led_external_D1.toggle()
            time.sleep(0.3)
        elif button2.value() == 0:
            disp.off()
            time.sleep(0.3)

        
        #print('loop elapsed ms ' + str(now_ms() - loop_start))
