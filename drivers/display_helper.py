class DisplayHelp():
    def __init__(self, display, color=1, line_length=16, line_heigth=12, debug=False):
        self.display = display
        self.color = color
        self.line_length = line_length
        self.line_heigth = line_heigth
        self.debug = debug

    def write_line(self, message, row):
        if len(message) > 16:
            print('message on line ' + str(row) + ' length truncated')
            message = message[0:16]
        if row == 1:
            self.display.text(message, 0, 0, 1)
        elif row == 2:
            self.display.text(message, 0, 16, 1)
        elif row == 3:
            self.display.text(message, 0, 28, 1)
        elif row == 4:
            self.display.text(message, 0, 40, 1)
        elif row == 5:
            self.display.text(message, 0, 52, 1)


    def write_block(self, message):

        line_height_sum = 0
        two_col_disp_line1_height = 16
        
        while (len(message) > 0):
            message.lstrip()
            if line_height_sum == 0:
                self.display.text(message[:self.line_length], 0, line_height_sum, 1)
                line_1 = 0
                if self.color == 1:
                    line_height_sum += self.line_heigth
                elif self.color == 2:
                    line_height_sum += two_col_disp_line1_height
                if self.debug:
                    print('inner ' + message[:self.line_length] + ' line_height: ' + str(line_height_sum))
                message = message[self.line_length:].lstrip()  
            else:
                self.display.text(message[:self.line_length], 0, line_height_sum, 1)
                line_height_sum += self.line_heigth
                if self.debug:
                    print('outer ' + message[:self.line_length] + ' line_height: ' + str(line_height_sum))
                message = message[self.line_length:].lstrip()                
        
              
    def clear(self):
        self.display.fill(0)
        
    def show(self):
        self.display.show()
        
    def dim(self):
        self.display.contrast(0)
        
    def wake(self):
        self.display.poweron()
        self.display.contrast(255)
        
    def off(self):
        self.display.poweroff()
    




